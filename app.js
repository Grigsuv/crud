const express = require('express');
const body_parser = require('body-parser');
const settings = require('./settings');
const app = express();

app.use(body_parser.json());
app.use(body_parser.urlencoded({
    extended: true
}));
app.use(express.static(__dirname + '/public'));
app.disable('x-powered-by');
const mongoose = require('mongoose');

let connection = mongoose.createConnection('localhost:27017/users');
mongoose.Promise = global.Promise;
require('./models/users')(settings);

app.db = {
    users: connection.model('users')
}

const ApiV1 = require('./controllers/api');
ApiV1.initialize(app);
const port = 3000;
app.listen(port, (err) => {
    if (err)
        console.log(err);
    else
        console.log('Server listening to port `' , port);
});