module.exports = {
    QUERY_OFFSET_DEFAULT_VALUE: 0,
    QUERY_LIMIT_DEFAULT_VALUE: 20,
    QUERY_LIMIT_MAXIMUM_VALUE: 100,
    Users : {
        password_minlength: 6,
        password_maxlength: 18,
        name_minlength: 3,
        name_maxlength: 12
    }
};

