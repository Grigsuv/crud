
const ErrorTypes = {
    USER_NOT_FOUND: 'user_not_found',
    INVALID_INPUT_PARAMS: 'input_input_params',
    UNKNOWN_ERROR: 'unknown_error'
};

class ResponseService {
    static generateError(type) {
        switch (type) {
            case ErrorTypes.USER_NOT_FOUND:
                return 'Failed to find users.';
            case ErrorTypes.INVALID_INPUT_PARAMS:
                return 'Invalid Input params'    
            default:
                return 'Unknown error';
        }
    }
}

module.exports = ResponseService;
module.exports.ErrorTypes = ErrorTypes;
