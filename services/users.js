const ResponseService = require('./response');
const ErrorTypes = ResponseService.ErrorTypes;

const AppSettings = require('./../settings');

class UsersService {

    constructor(app) {
        this.app = app;
    }

    getUsers(options) {

        return new Promise((resolve, reject) => {

            options = options || {};

            this.app.db.users.find({}).then(users => {
                users = users.map(u => {
                    return {
                        id: u._id,
                        username: u.username,
                        firstName: u.firstName,
                        lastName: u.lastName,
                        password: u.password,
                        status: u.status,
                        birthday: u.birthday
                    };
                });

                return resolve(users);
            }).catch(err => {
                console.log('getUsers: ', err);
                return reject(ResponseService.generateError(ErrorTypes.USER_NOT_FOUND));
            })
        });
    }


    addUser(options) {

        return new Promise((resolve, reject) => {

            options = options || {};

            if (!options.username)
                return reject(ResponseService.generateError(ErrorTypes.INVALID_INPUT_PARAMS));
            if (!options.password)
                return reject(ResponseService.generateError(ErrorTypes.INVALID_INPUT_PARAMS));

            let user = {
                username: options.username,
                password: options.password
            };
            if(options.firstname) 
                user.firstName = options.firstname;
            if(options.lastname)
                user.lastName  = options.lastname;
            if(options.birthday)
                user.birthday = options.birthday;
            if(options.status)
                user.status = options.status;

            this.app.db.users.create(user).then(data => {

                return resolve({
                    "username": data.username,
                    "id": data.id
                });
            }).catch(err => {
                console.log('addUser: ', err);
                return reject(ResponseService.generateError(ErrorTypes.UNKNOWN_ERROR));
            })
        });

    }

    updateUser(options) {
        return new Promise((resolve, reject) => {
            options = options || {};

            if(!options.id || !Number.isInteger(parseInt(options.id)))
                return reject(ResponseService.generateError(ErrorTypes.INVALID_INPUT_PARAMS))
            let id = parseInt(options.id);
        
            let fields = {};
            if (options.username)
                fields.username = options.username;
            if (options.password)
                fields.password = options.password;
            if (options.firstname)
                fields.firstName = options.firstname;
            if (options.lastname)
                fields.lastName = options.lastname;
            if(options.status)
                fields.status = options.status;
            if(options.birthday)
                fields.birthday = options.birthday;

            this.app.db.users.findByIdAndUpdate(id, {$set: fields}, {runValidators: true}).then(result => {
                return resolve({
                    id: result._id
                });
            }).catch(err => {
                console.log('addUser: ', err);
                return reject(ResponseService.generateError(ErrorTypes.UNKNOWN_ERROR));
            })

        });
    }
    deleteUser(options) {
        return new Promise((resolve, reject) => {
            options = options || {};
            if(!options.id)
                 return reject(ResponseService.generateError(ErrorTypes.INVALID_INPUT_PARAMS))

    
            let data = {
                '_id': options.id
            };

            this.app.db.users.findOneAndRemove(data).then(result => {

                return resolve({
                    "username": data.username,
                    "id": data.id
                });
            }).catch(err => {
                console.log('deleteUsers: ', err);
                return reject(ResponseService.generateError(ErrorTypes.UNKNOWN_ERROR));
            })

        });
    }
}

module.exports = UsersService;