const AppSettings = require('./../settings');

class Utility {
     parseQuery(req, res, next) {
        req.query.limit = parseInt(req.query.limit);
        if (req.query.limit > AppSettings.QUERY_LIMIT_MAXIMUM_VALUE) {
            req.query.limit = AppSettings.QUERY_LIMIT_DEFAULT_VALUE;
        }
        req.query.offset = parseInt(req.query.offset);
        if (!isFinite(req.query.offset)) {
            req.query.offset = AppSettings.QUERY_OFFSET_DEFAULT_VALUE;
        }

        next();
    }
}


module.exports = new Utility();