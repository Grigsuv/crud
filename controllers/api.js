const Utility = require('./../services/utility');
const UsersService = require('./../services/users');

class ApiV1 {

    initialize(app) {

        const usersService = new UsersService(app);

       app.use(Utility.parseQuery);
       
        app.get('/', (req, res) => {

            res.send('start page');
        })
        
        app.get('/api/users', (req, res) => {

            usersService.getUsers(req.query).then(users => {
                return res.send(users);
            }).catch(err => {
                console.log(err)
                return res.status(404).send(err);
            })
        });
        app.post('/api/users', (req, res) => {
            usersService.addUser(req.body).then(result => {
                return res.send(result);
            }).catch(err => {
                console.log(err);
                return res.status(404).send(err);
            })
        });

        app.put('/api/users/', (req, res) => {

            usersService.updateUser(req.body).then(result => {
                res.send(result);
            }).catch(err => {   
                console.log(err);
                return res.status(404).send(err);
            })
        });

        app.delete('/api/users/', (req, res) => {
 
            usersService.deleteUser(req.body).then(result => {
                res.send(result);
            }).catch(err => {
                console.log(err);
                return res.status(404).send(err);
            })
        });
    }
}

module.exports = new ApiV1();
