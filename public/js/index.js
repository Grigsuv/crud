var $TABLE = $('#table');
var $BTN = $('#export-btn');
const url = 'http://127.0.0.1:3000/api/users/';
$('.table-add').click(function () {
  var $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');

  $TABLE.find('table').append($clone);
});


$BTN.click(function () {
  const Http = new XMLHttpRequest();
  Http.open("GET", url);
  Http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  Http.send();
  Http.onreadystatechange = () => {
    if (Http.status == 200 && Http.readyState == 4) {
      response = Http.responseText;
      response = JSON.parse(response);

    
      for (content of response) {
        let $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');
        let data = $clone[0].children;
 
        data[0].innerHTML = content.id;
        if (content.username)
          data[1].innerHTML = content.username;
        if (content.firstName)
          data[2].innerHTML = content.firstName;
        if (content.lastName)
          data[3].innerHTML = content.lastName;
        if (content.password)
          data[4].innerHTML = content.password;
        if (content.birthday)
          data[5].innerHTML = content.birthday;
        if (content.status)
          data[6].innerHTML = content.status;

        $TABLE.find('table').append($clone);
      }

    }
  }
});


$('.table-remove').click(function () {
  let data = this.parentNode.parentNode.children;
  let body = {
    id: data[0].innerHTML
  };

  const url = 'http://127.0.0.1:3000/api/users/';
  const Http = new XMLHttpRequest();
  Http.open("DELETE", url);
  Http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  Http.send(JSON.stringify(body));
  Http.onreadystatechange =  () => {
    if (Http.status == 200 && Http.readyState == 4) {
      $(this).parents('tr').detach();
    }
  }

});

function buttonAction(btn) {
  let data = btn.parentNode.parentNode.children;
  if (!data[0].innerHTML || data[0].innerHTML == '') {
    return AddUser(btn)
  } else {
    return UpdateRow(btn);
  }
}

function UpdateRow(btn) {
  let data = btn.parentNode.parentNode.children;
  let body = {
    id: data[0].innerHTML
  };
  if (data[1].innerHTML)
    body.username = data[1].innerHTML
  if (data[2].innerHTML)
    body.firstName = data[2].innerHTML;
  if (data[3].innerHTML)
    body.lastName = data[3].innerHTML;
  if (data[5].innerHTML)
    body.birthday = data[5].innerHTML;
  if (data[4].innerHTML)
    body.password = data[4].innerHTML;
  if (data[6].innerHTML)
    body.status = data[6].innerHTML;

  const Http = new XMLHttpRequest();
  Http.open("PUT", url);
  Http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  Http.send(JSON.stringify(body));
}

function AddUser(btn) {
  let data = btn.parentNode.parentNode.children;
  let body = {};
  if (data[1].innerHTML)
    body.username = data[1].innerHTML
  if (data[2].innerHTML)
    body.firstName = data[2].innerHTML;
  if (data[3].innerHTML)
    body.lastName = data[3].innerHTML;
  if (data[5].innerHTML)
    body.birthday = data[5].innerHTML;
  if (data[4].innerHTML)
    body.password = data[4].innerHTML;
  if (data[6].innerHTML)
    body.status = data[6].innerHTML;

  const Http = new XMLHttpRequest();
  Http.open("POST", url);
  Http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  Http.send(JSON.stringify(body));
  Http.onreadystatechange = function () {
    if (Http.status == 200 && Http.readyState == 4) {
      content = Http.responseText;
      content = JSON.parse(content)
      data[0].innerHTML = content.id;
    }
  }
}










