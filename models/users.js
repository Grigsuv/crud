const mongoose = require('mongoose');
const Schema  = mongoose.Schema;

module.exports = (AppSettings, UserValidator) => {
    const UsersSettings = AppSettings.Users;

    let UsersSchema = new Schema({

        username: {
            type: String,
            required: true,
            minlength: 4,
            maxlength: 20,
            lowercase: true
        },
        password: {
            type: String,
            default: null,
            minlength: UsersSettings.password_minlength,
            maxlength: UsersSettings.password_maxlength
        },
        firstName: {
            type: String,
            trim: true,
            minlength: UsersSettings.name_minlength,
            maxlength: UsersSettings.name_maxlength
        },
        lastName: {
            type: String,
            trim: true,
            minlength: UsersSettings.name_minlength,
            maxlength: UsersSettings.name_maxlength
        },
        birthday: String,
        status: {
            type: Boolean,
            default: false
        }
    });

    return mongoose.model('users', UsersSchema);
}